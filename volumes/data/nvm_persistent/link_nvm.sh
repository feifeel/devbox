#!/bin/bash
# Each time you need to build a new devbox container, you no longer need to reinstall all nodejs versions or packages you installed before, they remain in the data volume. You just need to link them inside your container.

# link nodejs versions list
ln -s /data/nvm_persistent/versions ~/.nvm/
