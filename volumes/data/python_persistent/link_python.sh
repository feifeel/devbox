#!/bin/bash
# Each time you need to build a new devbox container, you no longer need to reinstall all python versions or packages you installed before, they remain in the data volume. You just need to link them inside your container.

# link python versions list
rmdir ~/.pyenv/versions
ln -s /data/python_persistent/versions ~/.pyenv

# link virtualenvs
ln -s /data/python_persistent/share ~/.local/
