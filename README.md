# devbox

Developmement environment in container.
The default os is Ubuntu

## Usage

### Pre-requirement
- Install docker and docker-compose
- Create or copy your ssh public key to dev_rsa.pub (example in devprox/dockerfiles/ubuntu/)

### build and start
```bash
cd devbox
docker-compose build
docker-compose up -d
```

### connect to your devbox
```bash
ssh dev@127.0.0.1 -p 13333
```
